var webpack = require('webpack'),
ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'eval-source-map',
    entry: [
        'babel-polyfill',
        'webpack/hot/only-dev-server',
        './src/app.js'
    ],
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
              test: /\.jsx?$/,
              exclude: /node_modules/, 
              loader: 'babel-loader',
              options: {
                plugins: ['transform-runtime', 'transform-decorators-legacy'],
                presets: ['es2015', 'stage-0', 'react']
              }
            },
            {
                test: /\.css$/,
                exclude: [/node_modules/, /styles/],
                loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                // loader: ExtractTextPlugin.extract({
                //     fallback: 'style-loader',
                //     use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                // })
            },
            {
                test: /\.css$/,
                exclude: [/node_modules/, /src/],
                loader: 'style-loader!css-loader'
            }
        ]
    },
    plugins: [
        // new ExtractTextPlugin('app.css'),
        // new ExtractTextPlugin('[name].[contenthash].css'),
        new webpack.NoEmitOnErrorsPlugin()
    ]
};