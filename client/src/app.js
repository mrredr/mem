import 'babel-polyfill';
import 'isomorphic-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import client from './apolloClient';
import storeConfig from './storeConfig';
import MaterialContainer from './containers/MaterialContainer';
import MemContainer from './containers/MemContainer';
import QuestionContainer from './containers/QuestionContainer';
import MemQuestionsContainer from './containers/MemContainer/memQuestions';
import AnswerContainer from './containers/AnswerContainer';
// import '../styles/style.css'

const store = storeConfig();

class App extends React.Component {

  render() {
    let materialPcId = '58bd397a3ec0b83144162f0d';
    let questionPcId = '58d1788454a4b030185b7450';

    let materialMacId = '594643e94d0c3be5680ee5c0';
    let memMacId = '59464db8c44dbb83d6ce46a4';
    let questionMacId = '5946518dc44dbb83d6ce46a5';
    let answerMacId = '59dc96ad40b12e6adb7caf25';

    return(
      <ApolloProvider store={store} client={client}>
        {/* <QuestionContainer id={questionMacId}></QuestionContainer> */}
        {/* <MemContainer id={memMacId}></MemContainer> */}
        {/* <MemQuestionsContainer id={memMacId}></MemQuestionsContainer> */}
        {<MaterialContainer id={materialMacId}></MaterialContainer>}
        {/* <AnswerContainer id={answerMacId}></AnswerContainer> */}
      </ApolloProvider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));