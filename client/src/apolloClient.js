import ApolloClient, {
  createNetworkInterface,
  addTypeName,
} from 'apollo-client';
import Config from './config';

const client = new ApolloClient({
    networkInterface: createNetworkInterface({ uri: Config.graphqlURL }),
    queryTransformer: addTypeName,
});
export default client;