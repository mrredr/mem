import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import createImmutable from 'redux-immutable-state-invariant';
// import mems from './modules/mems';
import client from './apolloClient';

const loggerMiddleware = createLogger(); // initialize logger
const immutableMiddlware = createImmutable();
const middlewares = [loggerMiddleware, immutableMiddlware, thunk, client.middleware()];
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore); // apply logger to redux

const reducer = combineReducers({
  // mems,
  apollo: client.reducer(),
});

const storeConfig = (initialState) => createStoreWithMiddleware(reducer, initialState);
export default storeConfig;