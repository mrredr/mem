import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './index.module.css';
import { Text } from '../../components';


const Material = ({
    material,
    children,
    onUpdate,
}) => (
    <div className={styles.material}>
        <h1>
            <Text
                html={material.title}
                disabled={false}
                onUpdate={onUpdate}
            />
        </h1>
        {children}
    </div>
);

Material.propTypes = {
    material: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        mems: PropTypes.array,
    }),
    children: PropTypes.node,
};

export default CSSModules(Material, styles);