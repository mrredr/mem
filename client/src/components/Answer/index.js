import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './index.module.css';
import { Text } from '../../components';

const Answer = ({
    html,
    disabled,
    onUpdate,
    children,
}) => (
    <div className={styles.answer}>
        <Text
            html={html}
            disabled={disabled}
            onUpdate={onUpdate}
        />
        {children}
    </div>
);

Answer.propTypes = {
    html: PropTypes.string,
    disabled: PropTypes.bool,
    onUpdate: PropTypes.func,
};

export default CSSModules(Answer, styles);