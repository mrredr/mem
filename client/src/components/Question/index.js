import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './index.module.css';
import { Text } from '../../components';

const Question = ({
    html,
    disabled,
    onUpdate,
    onRemove,
    onToggleAnswerVisibility,
    isAnswerVisible,
    children,
}) => (
    <div className={styles.question}>
        <Text
            html={html}
            disabled={disabled}
            onUpdate={onUpdate}
            onRemove={onRemove}
        />
        <div onClick={onToggleAnswerVisibility}>
            {isAnswerVisible? 'Hide answer': 'Show answer'}
        </div>
        {children}
    </div>
);

Question.propTypes = {
    html: PropTypes.string,
    disabled: PropTypes.bool,
    onUpdate: PropTypes.func,
    onRemove: PropTypes.func,
    isAnswerVisible: PropTypes.bool,
    onToggleAnswerVisibility: PropTypes.func,
};

export default CSSModules(Question, styles);