import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ContentEditable from 'react-contenteditable';
import _debounce from 'lodash/debounce';

import styles from './index.module.css';
import Config from '../../config';

class Text extends Component {
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown= this.handleKeyDown.bind(this);
        this.updateText = _debounce(this.updateText, Config.debounceInterval);
    }

    render() {
        const {
            html,
            disabled,
        } = this.props;

        return (
            <ContentEditable
                className={styles.text}
                html={html}
                disabled={disabled}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
            />
        );
    }

    handleChange(e) {
        if (this.props.onChange) {
            this.props.onChange(e);
        }
        e.persist();
        this.updateText(e);
    }

    updateText(e) {
        this.props.onUpdate(e);
    }

    handleKeyDown(e) {
        switch(e.key) {
            case 'Backspace':
                if (e.target.textContent.length === 0 && this.props.onRemove) {
                    this.props.onRemove();
                }
                break;
            default:
                break;
        }
    }
}

Text.propTypes = {
    html: PropTypes.string,
    disabled: PropTypes.bool,
    onKeyDown: PropTypes.func,
    onChange: PropTypes.func,   //Immediate update
    onUpdate: PropTypes.func,   //Debounced update
};

export default Text;