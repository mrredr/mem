import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './index.module.css';
import { Text } from '../../components';

const Loading = () => (
    <div className={styles.loading}>
        Loading...
    </div>
);

export default CSSModules(Loading, styles);