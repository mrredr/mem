import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './index.module.css';
import { Text } from '../../components';

const Mem = ({
    html,
    disabled,
    onUpdate,
    onRemove,
    children,
    onToggleVisibility,
    isQuestionsVisible,
}) => (
    <div className={styles.mem}>
        <Text
            html={html}
            disabled={disabled}
            onUpdate={onUpdate}
            onRemove={onRemove}
        />
        <div className={styles.buttonMoreLess} onClick={onToggleVisibility}>
            {isQuestionsVisible ? '/\\Q': 'vQ'}
        </div>
        {children}
    </div>
);

Mem.propTypes = {
    html: PropTypes.string,
    disabled: PropTypes.bool,
    onUpdate: PropTypes.func,
    onRemove: PropTypes.func,
    onToggleVisibility: PropTypes.func,
    isQuestionsVisible: PropTypes.bool,
};

export default CSSModules(Mem, styles);