import React, {Component} from 'react';

import { Text, Answer, Loading } from '../../components';
import { updateAnswer, getAnswer } from './graphql'

class AnswerContainer extends Component {
    constructor(props) {
        super(props);
        const {
            questionId,
        } = this.props;

        this.handleUpdate = this.handleUpdate.bind(this);
    }

    render() {
        const {
            id,
            answer,
            loading,
        } = this.props;

        return loading?
            (
                <Loading/>
            ):(
                <Answer
                    html={answer.content}
                    disabled={false}
                    onUpdate={this.handleUpdate}/>
            );
    }

    handleUpdate(e) {
        const {
            id,
            updateAnswer,
        } = this.props;

        updateAnswer({
            answerId: id,
            content: e.target.value,
        });
    }
}

export default getAnswer(updateAnswer(AnswerContainer));