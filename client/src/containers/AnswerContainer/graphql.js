import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const GetAnswer = gql`
    query Answer($id: String) {
        answer(id: $id) {
            id
            content
        }
    }
`;

export const getAnswer = graphql(GetAnswer, {
  	props: ({ data: { answer, loading } }) => ({
    	answer, loading,
  	}),
  	skip: (ownProps) => ownProps.answer,
});

const UpdateAnswer = gql`
    mutation updateAnswer($answerId: String, $content: String) {
        updateAnswer(answerId: $answerId, content: $content) {
            id
            content
        }
    }
`;

export const updateAnswer = graphql(UpdateAnswer, {
    props: ({mutate}) => ({
        updateAnswer: ({answerId, content}) => mutate({variables: {answerId, content}}),
    }),
});