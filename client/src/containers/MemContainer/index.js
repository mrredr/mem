import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withApollo, graphql, compose } from 'react-apollo';

import { Mem, Loading } from '../../components';
import MemQuestionsContainer from './memQuestions';
import styles from './index.module.css';
import { getMem, updateMem } from './graphql';

class MemContainer extends Component {
    constructor(props) {
        super(props);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleToggleQuestionsVisibility = this.handleToggleQuestionsVisibility.bind(this);
        this.state = {
            isQuestionsVisible: false,
        };
    }

    render() {
        const {
            mem,
            loading,
        } = this.props;
        const {
            isQuestionsVisible,
        } = this.state;

        return loading?
            (
                <Loading/>
            ):
            (
            <Mem
                html={mem.content}
                disabled={false}
                onUpdate={this.handleUpdate}
                onRemove={this.handleRemove}
                onToggleVisibility={this.handleToggleQuestionsVisibility}
                isQuestionsVisible={isQuestionsVisible}>
                {isQuestionsVisible &&
                    <MemQuestionsContainer id={mem.id}/>
                }
            </Mem>
            );
    }

    handleUpdate(e) {
        const {
            id,
            updateMem
        } = this.props;

        updateMem({
            memId: id,
            content: e.target.value,
        });
    }

    handleRemove() {
        this.removeMemFromMaterial();
    }

    removeMemFromMaterial() {
        const {
            id,
            mem,
            onRemove,
        } = this.props;

        if (mem) {
            onRemove(id);
        }
    }

    handleToggleQuestionsVisibility() {
        this.setState({
            isQuestionsVisible: !this.state.isQuestionsVisible,
        });
    }
}

MemContainer.propTypes = {
    id: PropTypes.string,
    key: PropTypes.string,
    mem: PropTypes.object,
    updateMem: PropTypes.func,
    onRemove: PropTypes.func,
    loading: PropTypes.bool,
    fetchMore: PropTypes.func,
};

export default getMem(updateMem(MemContainer));