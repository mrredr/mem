import React, {Component} from 'react';
import QuestionContainer from '../QuestionContainer';
import {getMem, createQuestionInMem, deleteQuestionFromMem} from './graphql';

class MemQuestionsContainer extends Component {
    constructor() {
        super();
        this.handleAdd = this.handleAdd.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    render() {
        const {
            mem,
            loading,
            questions,
        } = this.props;

        return loading?
            (
                <div>Loading...</div>
            ):
            (
            <div>
                <div onClick={this.handleAdd}>
                    +Q
                </div>
                {mem.questions.map((question, i) =>
                    <QuestionContainer
                        id={question.id}
                        key={question.id}
                        question={question}
                        onRemove={this.handleRemove}
                    />
                )}
            </div>
            );
    }

    handleAdd() {
        const {
            createQuestionInMem,
            id,
        } = this.props;

        createQuestionInMem({
            memId: id,
            questionContent: '',
        });
    }

    handleRemove(questionId) {
        const {
            id,
            deleteQuestionFromMem,
        } = this.props;

        deleteQuestionFromMem({
            memId: id,
            questionId,
        });
    }
}

export default getMem(createQuestionInMem(deleteQuestionFromMem(MemQuestionsContainer)));
