import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const GetMem = gql`
    query Mem($id: ID) {
        mem(id: $id) {
            id
            content
            questions {
                id,
                content,
                answer {
                    id,
                    content
                }
            }
        }
    }
`;
export const getMem =  graphql(GetMem, {
    props: ({data: { mem, loading }}) => ({
        mem, loading,
    }),
    skip: (ownProps) => ownProps.mem,
});

const UpdateMem = gql`
    mutation updateMem($memId: String, $content: String) {
        updateMem(memId: $memId, content: $content) {
            id,
            content
        }
    }
`;
export const updateMem = graphql(UpdateMem, {
    props: ({mutate}) => ({
        updateMem: ({memId, content}) => mutate({variables: {memId, content}}),
    }),
});

const CreateQuestionInMem = gql`
    mutation createQuestionInMem($memId: String, $questionContent: String) {
        createQuestionInMem(memId: $memId, questionContent: $questionContent) {
            id,
            questions {
                id, content, answer { id, content }
            }
        }
    }
`;
export const createQuestionInMem = graphql(CreateQuestionInMem, {
    props: ({mutate}) => ({
        createQuestionInMem({memId, questionContent}) {
            return mutate({variables: {memId, questionContent}})
        }
    }),
});

const DeleteQuestionFromMem = gql`
    mutation deleteQuestionFromMem($memId: String, $questionId: String) {
        deleteQuestionFromMem(memId: $memId, questionId:$questionId) {
            id
            questions {id, content}
        }
    }
`;
export const deleteQuestionFromMem = graphql(DeleteQuestionFromMem, {
    props: ({mutate}) => ({
        deleteQuestionFromMem({questionId, memId}) {
            return mutate({variables: {questionId, memId}})
        }
    })
});