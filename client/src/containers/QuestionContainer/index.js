import React, { Component } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';
import CSSModules from 'react-css-modules';

import { Text, Question, Loading } from '../../components';
import AnswerContainer from '../AnswerContainer';
import {getQuestion, updateQuestion} from './graphql';
import styles from './index.module.css';

class QuestionContainer extends Component {
    constructor(props) {
        super(props);

        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleToggleAnswerVisibility = this.handleToggleAnswerVisibility.bind(this);
        this.state = {
            isAnswerVisible: false
        };
    }

    render() {
        const {
            loading,
            question,
            id,
        } = this.props;
        const {
            isAnswerVisible
        } = this.state;

        return loading?
            (
                <Loading/>
            ):(
            <Question
                html={question.content}
                disabled={false}
                onUpdate={this.handleUpdate}
                onRemove={this.handleRemove}
                onToggleAnswerVisibility={this.handleToggleAnswerVisibility}
                isAnswerVisible={this.isAnswerVisible}>
                {isAnswerVisible &&
                    <AnswerContainer id={question.answer.id} answer={question.answer}/>
                }
            </Question>
            );
    }

    handleUpdate(e) {
        const {
            id,
            updateQuestion,
        } = this.props;

        updateQuestion({
            questionId: id,
            content: e.target.value,
        })
    }

    handleRemove() {
        const {
            id,
            question,
            onRemove,
        } = this.props;

        if (question) {
            onRemove(id);
        }
    }

    handleToggleAnswerVisibility() {
        this.setState({
            isAnswerVisible: !this.state.isAnswerVisible
        });
    }
}

QuestionContainer.propTypes = {
    id: PropTypes.string,
    question: PropTypes.object,
    onRemove: PropTypes.func,
};

export default getQuestion(updateQuestion(QuestionContainer));