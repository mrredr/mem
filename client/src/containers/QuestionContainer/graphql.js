import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const GetQuestion = gql`
    query Question($id: ID) {
        question(id: $id) {
            id
            content,
            answer {
                id,
                content
            }
        }
    }
`;
export const getQuestion = graphql(GetQuestion, {
    props: ({data: {question, loading}}) => ({
        question, loading
    }),
    skip: (ownProps) => ownProps.question,
});

const UpdateQuestion = gql`
    mutation updateQuestion($questionId: ID, $content: String){
        updateQuestion(questionId: $questionId, content: $content) {
            id,
            content
        }
    }
`;
export const updateQuestion = graphql(UpdateQuestion, {
    props: ({mutate}) => ({
        updateQuestion: ({questionId, content}) => mutate({variables: {questionId, content}}),
    }),
});
