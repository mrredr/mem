import React, {Component} from 'react';
import MemContainer from '../MemContainer';
import {getMaterial, createMemInMaterial, deleteMemFromMaterial} from './graphql';

class MaterialMemsContainer extends Component {
    constructor(props) {
        super(props);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    render() {
        const {
            material,
            loading,
            mems,
        } = this.props;

        return loading?
            (
                <div>Loading...</div>
            ):
            (
            <div>
                {material.mems.map((mem, i) =>
                    <MemContainer
                        id={mem.id}
                        key={mem.id}
                        mem={mem}
                        onRemove={this.handleRemove}
                    />
                )}
                <div onClick={this.handleAdd}>Add mem[+]</div>
            </div>
            );
    }

    handleAdd() {
        const {
            id,
            createMemInMaterial
        } = this.props;
        createMemInMaterial({
            memContent: '',
            materialId: id,
        });
    }

    handleRemove(memId) {
        const {
            id,
            deleteMemFromMaterial,
        } = this.props;

        deleteMemFromMaterial({
            materialId: id,
            memId,
        });
    }
}

export default getMaterial(createMemInMaterial(deleteMemFromMaterial(MaterialMemsContainer)));