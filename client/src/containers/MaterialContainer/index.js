import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql, compose } from 'react-apollo';

import { Material, Loading } from '../../components';
import { MemContainer } from '../../containers';
import MaterialMemsContainer from './materialMems'
import { getMaterial, updateMaterial } from './graphql';

class MaterialContainer extends Component {
  constructor(props) {
    super(props);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  render() {
    const {
      material,
      loading,
      children,
    } = this.props;

    return loading?
      (
          <Loading/>
      ):
      (
          <Material material={material} onUpdate={this.handleUpdate}>
            <MaterialMemsContainer id={material.id}/>
          </Material>
      );
  }

  async handleUpdate(e) {
    const {
        id,
        updateMaterial
    } = this.props;
    updateMaterial({
        materialId: id,
        title: e.target.value,
    });
  }
}

MaterialContainer.propTypes = {
  material: PropTypes.object,
  loading: PropTypes.bool,
};

const MaterialWithData = compose(
  getMaterial,
  updateMaterial,
);

export default MaterialWithData(MaterialContainer);