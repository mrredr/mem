import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const GetMaterial = gql`
  query Material($id: ID) {
    material(id: $id) {
      id
      title
      mems {
        id
        content
      }
    }
  }
`;
export const getMaterial = graphql(GetMaterial, {
  props: ({ data: { material, loading } }) => ({
    material, loading,
  }),
  skip: (ownProps) => ownProps.material,
});

const UpdateMaterial = gql`
    mutation updateMaterial($materialId: String, $title: String) {
        updateMaterial(materialId: $materialId, title: $title) {
            id,
            title
        }
    }
`;

export const updateMaterial = graphql(UpdateMaterial, {
    props: ({mutate}) => ({
        updateMaterial({materialId, title}) {
            mutate({variables: {materialId, title}})
        }
    })
});

const CreateMemInMaterial = gql`
  mutation ($to: Int, $memContent: String, $materialId: String) {
    createMemInMaterial(to: $to, memContent: $memContent, materialId: $materialId) {
      id
      mems {id, content}
    }
  }
`;
export const createMemInMaterial = graphql(CreateMemInMaterial, {
  props: ({mutate}) => ({
    createMemInMaterial({to, memContent, materialId}) {
        mutate({variables: {to, memContent, materialId}})
    }
  })
});

const DeleteMemFromMaterial = gql`
  mutation ($memId: String, $materialId: String) {
    deleteMemFromMaterial(memId: $memId, materialId: $materialId) {
      id
      mems {id, content}
    }
  }
`;
export const deleteMemFromMaterial = graphql(DeleteMemFromMaterial, {
  props: ({mutate}) => ({
    deleteMemFromMaterial({memId, materialId}) {
        mutate({variables: {memId, materialId}})
      }
  })
});