import React, { Component, PropTypes } from 'react';


class Mem extends Component {
    static PropTypes = {
        mem: PropTypes.object,
        text: PropTypes.string
    }

    state = {
        editing: false,
        text: ''
    }

    completeEditing = e => {
        this.props.onSave(e.target.value)
        this.setState({editing: false, text: ''})
    }

    startEditing = e => {
        this.setState({editing: true, text: this.props.mem.title})
    }

    render() {
        const { mem } = this.props;
        return (
            <div>
            {!this.state.editing &&
                <h2
                    onDoubleClick={this.startEditing}>
                    {mem.title}
                </h2>
            }
            {this.state.editing &&
                <input
                    type="text"
                    autoFocus="true"
                    value={this.state.text}
                    onChange={e => this.setState({text:e.target.value})}
                    onBlur={this.completeEditing} />
            }
            </div>
        )
    }
}

export default Mem;