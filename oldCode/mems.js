const REQUEST_MEMS_OPENED = 'mems/REQUEST_OPENED';
const REQUEST_MEMS_SUCCEED = 'mems/REQUEST_SUCCEED';
const REQUEST_MEMS_FAILED = 'mems/REQUEST_FAILED';
const EDIT_MEM = 'mem/EDIT_MEM';

export const fetchMems = () => dispatch => {
    dispatch(requestMemsOpened());
    return fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(response => {
            dispatch(requestMemsSucceed(response));
        })
        .catch(error => {
            dispatch(requestMemsFailed(error));
        });
};

const requestMemsOpened = mems => ({
    type: REQUEST_MEMS_OPENED
});

const requestMemsSucceed = mems => ({
    type: REQUEST_MEMS_SUCCEED,
    result: mems
});

 const requestMemsFailed = error => ({
    type: REQUEST_MEMS_FAILED,
    error: error
});

export const editMem = (id, value) => ({
    type: EDIT_MEM,
    value,
    id
})

const initialState = {
    memsList: [],
    isLoading: false,
    error: null,
    another: {
        else: {
            bar: {}
        }
    }
}

export default function(state = initialState, action) {
    switch(action.type) {
        case REQUEST_MEMS_OPENED:
            return {
                ...state,
                loading: true
            };

        case REQUEST_MEMS_SUCCEED:
            return {
                ...state,
                memsList: action.result,
                loading: false
            };

        case REQUEST_MEMS_FAILED:
            return {
                ...state,
                memsList: [],
                error: action.error,
                loading: false
            };
        
        case EDIT_MEM:
            return {
                ...state,
                memsList: state.memsList.map(mem => 
                    mem.id === action.id?
                        {...mem, title: action.value} :
                        mem
                )
            };
        
        default:
            return state;
    }
}