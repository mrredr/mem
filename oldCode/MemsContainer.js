import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { fetchMems, editMem } from '../modules/mems';
import Mem from '../components/Mem';

class Mems extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        mems: PropTypes.array
    }

    componentWillMount() {
        const { dispatch } = this.props;
        dispatch(fetchMems());
    }

    save = (id, value) => {
        const { dispatch } = this.props;
        dispatch(editMem(id, value))
    }

    render() {
        const { mems } = this.props
        return (
            <div>
                {
                    mems.map((mem) =>
                        <Mem 
                            key={mem.id} 
                            mem={mem}
                            onSave={title => this.save(mem.id, title)}></Mem> 
                    )
                }
            </div>
        );
    }
}

export default connect(state => ({
    mems: state.mems.memsList
}))(Mems)