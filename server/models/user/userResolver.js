import { User } from './user';

export async function getUserById(root, {id}) {
    return await User.findById(id);;
}

export async function addUser(root, {username, password}) {
    let user = new User();
    user.username = username;
    user.password = password;
    await user.save();
    return user;
}

export async function updateUser(root, {userId, password}) {
    let user = await User.findById(userId);
    user.password = password || user.password;
    await user.save();
    return user;
}

export async function addMaterialToUser(root, {userId, materialId}) {
    let user = await User.findById(userId);
    user.materials.push(materialId);
    await user.save();
    return user;
}

export async function removeMaterialFromUser(root, {userId, materialId}) {
    let user = await User.findById(userId);
    user.materials = user.materials.filter(material => material.id !== materialId);
    await user.save();
    return user;
}
