import {
  GraphQLID
} from 'graphql';

import { UserType } from './user'
import { getUserById } from './userResolver'

let userQueries = {
    user: {
        type: UserType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: getUserById
    }
}

export default {
    user: userQueries.user
}