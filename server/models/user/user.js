import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull
 } from 'graphql'
import mongoose from 'mongoose';
import { Material, MaterialType } from '../material/material';

var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  materials: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Material'
  }]
}, {
  timestamps: true
});

var User = mongoose.model('User', UserSchema);

var UserType = new GraphQLObjectType({
  name: 'User',
  description: 'User creator',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the user.'
    },
    username: {
      type: GraphQLString,
      description: 'The username of the user.'
    },
    password: {
      type: GraphQLString,
      description: 'The password of the user.'
    },
    materials: {
      type: new GraphQLList(MaterialType),
      description: 'The materials of the user, or an empty list if they have none.',
      resolve: async (user, params, context, fieldsAST) => {
        await User.populate(user, 'materials');
        return user.materials;
      }
    }
  })
});

export { User, UserType }