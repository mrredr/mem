import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { UserType, User } from './user';
import * as Resolver from './userResolver'

let userMutations = {
    addUser: {
        type: UserType,
        args: {
            username: {
                type: new GraphQLNonNull(GraphQLString)
            },
            password: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: Resolver.addUser
    },
    updateUser: {
        type: UserType,
        args: {
            userId: {
                type: new GraphQLNonNull(GraphQLString)
            },
            password: {
                type: GraphQLString
            }
        },
        resolve: Resolver.updateUser
    },
    addMaterialToUser: {
        type: UserType,
        args: {
            userId: {
                type: new GraphQLNonNull(GraphQLString)
            },
            materialId: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: Resolver.addMaterialToUser
    },
    removeMaterialFromUser: {
        type: UserType,
        args: {
            userId: {
                type: new GraphQLNonNull(GraphQLString)
            },
            materialId: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: Resolver.removeMaterialFromUser
    }
}

export default  {
    addUser: userMutations.addUser,
    updateUser: userMutations.updateUser,
    addMaterialToUser: userMutations.addMaterialToUser,
    removeMaterialFromUser: userMutations.removeMaterialFromUser
}