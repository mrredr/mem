import {
  GraphQLID
} from 'graphql';

import { MaterialType, Material } from './material';
import { getMaterial } from './materialResolver';

let materialQueries = {
    material: {
        type: MaterialType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: getMaterial
    }
}

export default {
    material: materialQueries.material
}