import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
 } from 'graphql'
import mongoose from 'mongoose';
import { Mem, MemType } from '../mem/mem';
import { User, UserType } from '../user/user';

var MaterialSchema = new mongoose.Schema({
  title: {
    type: String
  },
  mems: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Mem'
  }],
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true
});

var Material = mongoose.model('Material', MaterialSchema);

var MaterialType = new GraphQLObjectType({
  name: 'Material',
  description: 'Material creator',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
      description: 'The id of the material.',
    },
    title: {
      type: GraphQLString,
      description: 'The title of the material.',
    },
    mems: {
      type: new GraphQLList(MemType),
      description: 'The mems of material',
      resolve: async(material, params, context, fieldsAST) => {
        await Material.populate(material, 'mems');
        return material.mems;
      }
    },
    creator: {
        type: UserType,
        description: 'Creator of material',
        resolve: async(material, params, context, fieldsAST) => {
          await Material.populate(material, 'creator');
          return material.creator;
        }
    }
  })
});

export { Material, MaterialType }