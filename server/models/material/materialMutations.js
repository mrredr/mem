import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { MaterialType, Material } from './material';
import { createMaterial, updateMaterial, createMemInMaterial, deleteMemFromMaterial, moveMemInMaterial } from './materialResolver';

let materialMutations = {
    createMaterial: {
        type: MaterialType,
        args: {
            title: {
                type: GraphQLString
            }
        },
        resolve: createMaterial
    },
    updateMaterial: {
        type: MaterialType,
        args: {
            materialId: {
                type: GraphQLString
            },
            title: {
                type: GraphQLString
            }
        },
        resolve: updateMaterial
    },
    createMemInMaterial: {
        type: MaterialType,
        args: {
            materialId: {
                type: GraphQLString
            },
            memContent: {
                type: GraphQLString
            },
            to: {
                type: GraphQLInt
            }
        },
        resolve: createMemInMaterial
    },
    deleteMemFromMaterial: {
        type: MaterialType,
        args: {
            materialId: {
                type: GraphQLString
            },
            memId: {
                type: GraphQLString
            }
        },
        resolve: deleteMemFromMaterial
    },
    moveMemInMaterial: {
        type: MaterialType,
        args: {
            materialId: {
                type: GraphQLString
            },
            memId: {
                type: GraphQLString
            },
            to: {
                type: GraphQLInt
            }
        },
        resolve: moveMemInMaterial
    }
}

export default {
    createMaterial: materialMutations.createMaterial,
    updateMaterial: materialMutations.updateMaterial,
    createMemInMaterial: materialMutations.createMemInMaterial,
    deleteMemFromMaterial: materialMutations.deleteMemFromMaterial,
    moveMemInMaterial: materialMutations.moveMemInMaterial
}