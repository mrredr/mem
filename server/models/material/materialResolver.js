import mongoose from 'mongoose';
import { Material } from './material';
import { Mem } from '../mem/mem';

export async function getMaterial(root, {id}) {
    return await Material.findById(id);
}

export async function createMaterial(obj, {title}) {
    let material = new Material();
    material.title = title;
    material.mems = [];
    return await material.save();
}

export async function updateMaterial(root, {materialId, title}) {
    let material = await Material.findById(materialId);
    material.title = title || material.title;
    await material.save();
    return material;
}

export async function createMemInMaterial(obj, {materialId, memContent='', to}) {
    let material = await Material.findById(materialId);

    let mem = new Mem();
    mem.content = memContent;
    await mem.save();
    to = to || material.mems.length;
    material.mems.splice(to, 0, mem);

    return await material.save();
}

export async function deleteMemFromMaterial(obj, {materialId, memId}) {
    let material = await Material.findById(materialId);

    let index = material.mems.findIndex((mem) => mem == memId);
    material.mems.splice(index, 1);
    Mem.findByIdAndRemove(memId);

    return await material.save();
}

export async function moveMemInMaterial(obj, {materialId, memId, to}) {
    let material = await Material.findById(materialId);

    let from = material.mems.findIndex((id) => id == memId);
    material.mems.splice(to, 0, material.mems.splice(from, 1)[0]);

    return await material.save();
}