import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { AnswerType } from './answer';
import {
    addOrUpdateAnswer,
    updateAnswer
 } from './answerResolver';

let answerMutations = {
    updateAnswer: {
        type: AnswerType,
        args: {
            answerId: {
                type: GraphQLString
            },
            content: {
                type: GraphQLString
            }
        },
        resolve: updateAnswer
    }
}

export default {
    updateAnswer: answerMutations.updateAnswer
}