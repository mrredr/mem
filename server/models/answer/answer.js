import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
 } from 'graphql'
import GraphQLDate from 'graphql-date';
import mongoose from 'mongoose';
import { UserType } from '../user/user';
import { QuestionType, Question } from '../question/question';

let AnswerSchema = new mongoose.Schema({
    content: {
        type: mongoose.Schema.Types.String
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    question: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Question'
    }
}, {
    timestamps: true
});

let Answer = mongoose.model('Answer', AnswerSchema);

let AnswerType = new GraphQLObjectType({
    name: 'Answer',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the answer',
        },
        content: {
            type: GraphQLString,
            description: 'Content of answer'
        },
        creator: {
            type: UserType,
            description: 'Creator of answer',
            resolve: async(answer, params, context, fieldsAST) => {
                await Answer.populate(answer, 'creator');
                return answer.creator;
            }
        },
        question: {
            type: QuestionType,
            description: 'Question where is this answer',
            resolve: async(answer, params, context, fieldsAST) => {
                await Question.populate(answer, 'question');
                return answer.question;
            }
        }
    })
})

export { Answer, AnswerType }