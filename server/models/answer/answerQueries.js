import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { AnswerType } from './answer';
import {
    getAnswer
 } from './answerResolver';

let answerQueries = {
    answer: {
        type: AnswerType,
        args: {
            id: {
                type: GraphQLString
            }
        },
        resolve: getAnswer
    },
}

export default {
    answer: answerQueries.answer,
}