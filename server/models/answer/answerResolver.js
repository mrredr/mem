import mongoose from 'mongoose';
import { Answer } from './answer';

export async function getAnswer(root, {id}) {
    return await Answer.findById(id);
}

export async function updateAnswer(root, {answerId, content}) {
    let answer = await Answer.findById(answerId);
    answer.content = content || answer.content;
    await answer.save();
    return answer;
}