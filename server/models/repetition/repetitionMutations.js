import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';
import GraphQLDate from 'graphql-date';

import { RepetitionType } from './repetition';
import { 
    addPoint    
 } from './repetitionResolver';

let repetitionMutations = {
    addPoint: {
        type: RepetitionType,
        args: {
            repetitionId: {
                type: GraphQLString
            },
            memId: {
                type: GraphQLString
            },
            userId: {
                type: GraphQLString
            },
            time: {
                type: GraphQLDate
            },
            level: {
                type: GraphQLInt
            }
        },
        resolve: addPoint
    }
}

export default {
    addPoint: repetitionMutations.addPoint
}