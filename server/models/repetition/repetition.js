import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
 } from 'graphql'
import GraphQLDate from 'graphql-date';
import mongoose from 'mongoose';
import { UserType, User } from '../user/user';
import { MemType, Mem } from '../mem/mem';

let RepetitionSchema = new mongoose.Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    mem: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Mem'
    },
    points: [{
        time: mongoose.Schema.Types.Date,
        level: mongoose.Schema.Types.Number
    }]
});

let Repetition = mongoose.model('Repetition', RepetitionSchema);

let PointType = new GraphQLObjectType({
    name: 'Point',
    fields: () => ({
        time: {
            type: GraphQLDate,
            description: 'Point time'
        },
        level: {
            type: GraphQLInt,
            description: 'Level of point'
        }
    })
});
let RepetitionType = new GraphQLObjectType({
    name: 'Repetition',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the repetition.',
        },
        points: {
            type: new GraphQLList(PointType),
            description: 'Array of points'
        },
        creator: {
            type: UserType,
            description: 'Creator of question',
            resolve: async(repetition) => {
                await User.populate(repetition, 'creator');
                return repetition.creator;
            }
        },
        mem: {
            type: MemType,
            description: 'The mem for repeating',
            resolve: async(repetition, params, context, fieldsAST ) => {
                await Mem.populate(repetition, 'mem');
                return repetition.mem;
            }
        }
    })
})

export { Repetition, RepetitionType }