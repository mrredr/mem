import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { RepetitionType } from './repetition';
import { 
    getRepetitionById,
    getRepetitionByUserAndMem
 } from './repetitionResolver';

let repetitionQueries = {
    repetition: {
        type: RepetitionType,
        args: {
            id: {
                type: GraphQLString
            }
        },
        resolve: getRepetitionById
    },
    getRepetitionByUserAndMem: {
        type: RepetitionType,
        args: {
            userId: {
                type: GraphQLString
            },
            memId: {
                type: GraphQLString
            }
        },
        resolve: getRepetitionByUserAndMem
    }
}

export default {
    repetition: repetitionQueries.repetition,
    getRepetitionByUserAndMem: repetitionQueries.getRepetitionByUserAndMem
}