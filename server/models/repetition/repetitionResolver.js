import mongoose from 'mongoose';

import { Repetition } from './repetition';

export async function getRepetitionById(root, {id}) {
    return await Repetition.findById(id);
}

export async function getRepetitionByUserAndMem(root, {userId, memId}) {
    return await Repetition.findOne({creator: userId, mem: memId});
}

export async function addPoint(root, {repetitionId, userId, memId, time, level}) {
    let repetition = null;

    if (repetitionId) {
        repetition = await Repetition.findById(repetitionId);
    } else {
        repetition = await Repetition.findOne({creator: userId, mem: memId});
    }

    if (!repetition) {
        repetition = new Repetition();
        repetition.creator = userId;
        repetition.mem = memId;
        repetition.points = [];
    }
    repetition.points.push({time, level});
    
    return await repetition.save();
}