import {
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { MemType, Mem } from './mem';
import {
    addMem,
    updateMem,
    createQuestionInMem,
    deleteQuestionFromMem
} from './memResolver';

import { QuestionType } from '../question/question';

let memMutations = {
    addMem: {
        type: MemType,
        args: {
            content: {
                type: GraphQLString
            }
        },
        resolve: addMem
    },
    updateMem: {
        type: MemType,
        args: {
            memId: {
                type: GraphQLString
            },
            content: {
                type: GraphQLString
            }
        },
        resolve: updateMem
    },
    createQuestionInMem: {
        type: MemType,
        args: {
            memId: {
                type: GraphQLString
            },
            questionId: {
                type: GraphQLString
            },
            questionContent: {
                type: GraphQLString
            }
        },
        resolve: createQuestionInMem
    },
    deleteQuestionFromMem: {
        type: MemType,
        args: {
            memId: {
                type: GraphQLString
            },
            questionId: {
                type: GraphQLString
            }
        },
        resolve: deleteQuestionFromMem
    }
}

export default {
    addMem: memMutations.addMem,
    updateMem: memMutations.updateMem,
    createQuestionInMem: memMutations.createQuestionInMem,
    deleteQuestionFromMem: memMutations.deleteQuestionFromMem
}