import mongoose from 'mongoose';
import { Mem } from './mem';
import { Question } from '../question/question';

export async function getMem(root, {id}) {
    return await Mem.findById(id);
}

export async function addMem(obj, {content}) {
    let mem = new Mem();
    mem.content = content;
    return await mem.save();
}

export async function updateMem(root, {memId, content}) {
    let mem = await Mem.findById(memId);
    mem.content = content !== undefined? content: mem.content;
    await mem.save();
    return mem;
}

export async function createQuestionInMem(root, {memId, questionContent=''}) {
    let mem = await Mem.findById(memId);

    let question = new Question();
    question.content = questionContent;
    await question.save();
    mem.questions.push(question.id);

    return await mem.save();
}

export async function deleteQuestionFromMem(root, {memId, questionId}) {
    let mem = await Mem.findById(memId);
    let index = mem.questions.findIndex(question => question == questionId);
    mem.questions.splice(index, 1);
    Question.findByIdAndRemove(questionId);
    return await mem.save();
}