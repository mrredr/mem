import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull
 } from 'graphql'
import mongoose from 'mongoose';

import { QuestionType } from '../question/question';
import { RepetitionType } from '../repetition/repetition';
import { UserType } from '../user/user';

let MemSchema = new mongoose.Schema({
    content: {
        type: String
    },
    questions: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Question'
    }],
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

let Mem = mongoose.model('Mem', MemSchema);

var MemType = new GraphQLObjectType({
  name: 'Mem',
  description: 'Mem creator',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the mem.',
    },
    content: {
      type: GraphQLString,
      description: 'The content of the mem.',
    },
    questions: {
        type: new GraphQLList(QuestionType),
        description: 'Questions of mem',
        resolve: async (mem, params, context, fieldsAST) => {
            await Mem.populate(mem, 'questions');
            return mem.questions;
        }
    },
    creator: {
        type: UserType,
        description: 'Creator of mem',
        resolve: async(mem, params, context, fieldsAST) => {
          await Mem.populate(mem, 'creator');
          return mem.creator;
        }
    }
  })
});

export { Mem, MemType }