import {
  GraphQLID
} from 'graphql';

import { MemType } from './mem'
import { getMem } from './memResolver'

let memQueries = {
    mem: {
        type: MemType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: getMem
    }
}

export default {
    mem: memQueries.mem
}