import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull
 } from 'graphql'
import mongoose from 'mongoose';
import { Answer, AnswerType } from '../answer/answer';
import { UserType } from '../user/user';

let QuestionSchema = new mongoose.Schema({
    content: {
        type: String
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

let Question = mongoose.model('Question', QuestionSchema);

let QuestionType = new GraphQLObjectType({
    name: 'Question',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the question.',
        },
        content: {
            type: GraphQLString,
            description: 'Content of question'
        },
        creator: {
            type: UserType,
            description: 'Creator of question',
            resolve: async(question, params, context, fieldsAST) => {
                await Question.populate(question, 'creator');
                return question.creator;
            }
        },
        answer: {
            type: AnswerType,
            description: 'Answer for question from this user',
            resolve: async(question, params, context, fieldAST) => {
                let answer = await Answer.find({question: question.id});
                if (!answer.length) {
                   answer = new Answer();
                   answer.question = question.id;
                   return await answer.save();
                } else {
                   return answer[0];
                }
            }
        },
        answers: {
            type: new GraphQLList(AnswerType),
            description: 'Answers of question',
            resolve: async(question, params, context, fieldAST) => {
                await Question.populate(question, 'answers');
                return question.answers;
            }
        }
    })
});

export { Question, QuestionType }