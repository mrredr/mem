import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { AnswerType } from '../answer/answer';
import { QuestionType } from './question';
import { updateQuestion } from './questionResolver';

let questionMutations = {
    updateQuestion: {
        type: QuestionType,
        args: {
            questionId: {
                type: GraphQLID
            },
            content: {
                type: GraphQLString
            }
        },
        resolve: updateQuestion
    }
}

export default {
    updateQuestion: questionMutations.updateQuestion,
}