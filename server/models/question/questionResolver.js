import mongoose from 'mongoose';
import { Question } from './question';
import { Answer } from '../answer/answer';

export async function getQuestion(root, {id}) {
    return await Question.findById(id);
}

export async function updateQuestion(root, {questionId, content}) {
    let question = await Question.findById(questionId);
    question.content = content || question.content;
    await question.save();
    return question;
}
