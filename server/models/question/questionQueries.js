import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
} from 'graphql';

import { QuestionType } from './question'
import { getQuestion } from './questionResolver'

let questionQueries = {
    question: {
        type: QuestionType,
        args: {
            id: {
                type: GraphQLID
            }
        },
        resolve: getQuestion
    }
}

export default {
    question: questionQueries.question
}