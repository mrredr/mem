import {
  GraphQLObjectType
 } from 'graphql'

 import userQueries from './models/user/userQueries';
 import materialQueries from './models/material/materialQueries';
 import memQueries from './models/mem/memQueries';
 import questionQueries from './models/question/questionQueries';
 import answerQueries from './models/answer/answerQueries';
 import repetitionQueries from './models/repetition/repetitionQueries';

 export default new GraphQLObjectType({
    name: 'Query',
    description: 'root for all api requests',
    fields: () => (
        Object.assign(
            userQueries,
            materialQueries,
            memQueries,
            questionQueries,
            answerQueries,
            repetitionQueries
        )
    )
 });