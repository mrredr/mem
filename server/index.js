import {
    GraphQLSchema
} from 'graphql'
import express from 'express';
import mongoose from 'mongoose';
import graphqlHTTP from 'express-graphql';
import CONFIG from './config';
import query from './rootQuery';
import mutation from './rootMutation';
import cors from 'cors';


class Server {
    app;
    port;
    schema;

    constructor() {
        this.port = process.env.PORT || 5000;
        this.app = express();
        this.dbURL = CONFIG.databaseURL;
        this.schema = new GraphQLSchema({
            query,
            mutation
        });
        mongoose.Promise = global.Promise;

        this.route();
        this.listen();

    }

    route() {
        this.app.use('/graphql', cors(), graphqlHTTP({ schema: this.schema, pretty: true, graphiql: true }))
    }

    listen() {
        mongoose.connect(this.dbURL);

        this.app.listen(this.port);
    }

}

var server = new Server();