import {
  GraphQLObjectType
 } from 'graphql'

 import materialMutations from './models/material/materialMutations';
 import userMutations from './models/user/userMutations';
 import memMutations from './models/mem/memMutations';
 import questionMutations from './models/question/questionMutations';
 import answerMutations from './models/answer/answerMutations';
 import repetitionMutations from './models/repetition/repetitionMutations';

 export default new GraphQLObjectType({
    name: 'Mutation',
    description: 'root mutations for all api requests',
    fields: () => (
        Object.assign(
            userMutations,
            materialMutations,
            memMutations,
            questionMutations,
            answerMutations,
            repetitionMutations
        )
    )
 });
